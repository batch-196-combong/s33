const express = require("express");

const app = express();

app.use(express.json())
// express.json() receve data and automatically parse the incoming jSON from the request

// app.use() is a method used to run another function or method for our expressjs api.
// it is used to run middlewares.

const port = 4000;

// Mock Collection for Courses
let courses = [
	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn ReactJS",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}
];

let users = [
	{
		email: "marybell_knight",
		password: "merrymarybell"
	},
	{
		email: "janedoePriest",
		password: "jobPriest100"
	},
	{
		email: "kimTofu",
		password: "dubuTofu"
	}
]

// creating route in express
// access express to have access to its route methods.
/*
	app.method() is a method similar to response.end()
	it automatically put headers to the response.	
*/ 

app.get('/',(req,res)=>{

	res.send("Hello from our first ExpressJS route")
})

app.post('/courses',(req,res)=>{

	// Note: Every time you need to access or use the request bidy, log it in the console. 
	courses.push(req.body);
	res.send(courses);
})

app.put('/',(req,res)=>{
	res.send("Hello from a PUT Method Route!")
})

app.delete('/',(req,res)=>{
	res.send(courses)
})

app.get('/users',(req,res)=>{

	res.send(users);
})

app.post('/users',(req,res)=>{
 
	users.push(req.body);
	res.send(users);
})

app.delete('/users',(req,res)=>{
	users.pop();
	console.log(users); //to check if the user has been deleted, i printed the users in the terminal
	res.send("The last user has been deleted successfully")
})


app.listen(port,() => console.log(`Express API running at port 4000`))

